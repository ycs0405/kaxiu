package com.kaxiu.config.spring;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * @Author: LiYang
 * @created on: 2018/8/31 1:46
 * @discribe:
 */
public class CustomLongConverter extends StdSerializer<Long> {
    public CustomLongConverter() {
        super(Long.class);
    }

    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value.toString().length() > 12) {
            gen.writeString(value.toString());
        } else {
            gen.writeNumber(value);
        }
    }
}
