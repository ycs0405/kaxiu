package com.kaxiu.service;

import com.kaxiu.persistent.entity.Attestation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kaxiu.vo.AttestationVo;

/**
 * <p>
 * 认证记录 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IAttestationService extends IService<Attestation> {

    int saveAttestation(AttestationVo vo);

}
