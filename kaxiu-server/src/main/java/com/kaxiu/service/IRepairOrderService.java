package com.kaxiu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kaxiu.persistent.entity.OrderItem;
import com.kaxiu.persistent.entity.RepairOrder;

/**
 * <p>
 * 订单项 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IRepairOrderService extends IService<RepairOrder> {

    RepairOrder getByOrderId(Long orderId);

}
