package com.kaxiu.service;

import com.kaxiu.persistent.entity.AccountWithdraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 提现 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IAccountWithdrawService extends IService<AccountWithdraw> {

}
