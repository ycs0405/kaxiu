package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.SysRole;
import com.kaxiu.persistent.mapper.SysRoleMapper;
import com.kaxiu.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private SysRoleMapper roleMapper;

    @Override
    public List<SysRole> getByUserId(Long id) {
        return roleMapper.selectByUserId(id);
    }
}
