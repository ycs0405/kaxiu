package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 维修人员信息表
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ServicePersonal extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 关联微信用户Open ID
     */
    private Long userId;

    /**
     * 接单数量
     */
    private Long orderNumber;

    /**
     * 累计评分
     */
    private Integer starLevelTotal;

    /**
     * 服务开始时间
     */
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    private LocalDateTime serviceEndTime;

    /**
     * 接单范围
     */
    private String serviceLocation;


}
