package com.kaxiu.persistent.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kaxiu.persistent.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kaxiu.persistent.entity.RepairOrder;
import com.kaxiu.vo.order.OrderResponseVo;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface OrdersMapper extends BaseMapper<Orders> {

    /**
     * consumer 获取自己的orders
     * @param page
     * @param userId
     * @param status
     * @return
     */
    IPage<OrderResponseVo> selectOrderList(Page page, @Param("userId") Serializable userId,
                                           @Param("status") Integer status);

    /**
     * consumer 获取order详情
     * @param orderId
     * @return
     */
    OrderResponseVo selectOrderById(@Param("orderId") Serializable orderId);

    /**
     * 获取有效订单列表
     * @param orderId
     * @return
     */
    List<OrderResponseVo> selectEffectiveOrderList(@Param("orderId") Long orderId);

    /**
     *  server 获取自己的orders
     * @param page
     * @param userId
     * @param status
     * @return
     */
    IPage<OrderResponseVo> selectServerOrders(Page page, @Param("userId") Serializable userId,
                                             @Param("status") Integer status);

}
