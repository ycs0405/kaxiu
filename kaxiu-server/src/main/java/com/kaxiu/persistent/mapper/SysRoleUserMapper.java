package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色中间表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}
