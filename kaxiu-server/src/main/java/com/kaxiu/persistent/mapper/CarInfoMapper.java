package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.CarInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 车辆信息
 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface CarInfoMapper extends BaseMapper<CarInfo> {

}
