package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.SysPermissionRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限中间表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface SysPermissionRoleMapper extends BaseMapper<SysPermissionRole> {

}
