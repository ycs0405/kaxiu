package com.kaxiu.web.common;

import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.util.StringUtil;
import com.kaxiu.vo.ResultDataWrap;
import lombok.var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;
import java.util.Random;

/**
 * @author: LiYang
 * @create: 2019-08-09 23:37
 * @Description:
 **/
@RequestMapping("/kaxiu/")
@Controller
public class UtilController extends AbstractBaseController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${kaxiu.resourceLocation}")
    private String resourceLocation;

    @Resource
    private RedisService redisService;

    private final ResourceLoader resourceLoader;

    @Autowired
    public UtilController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @RequestMapping(value = "imageUpload", produces = "application/json; charset=utf-8" ,method = RequestMethod.POST)
    @ResponseBody
    public ResultDataWrap imgUpdate(@RequestParam(value = "file") MultipartFile file) {
        if (file.isEmpty()) {
            return buildFailResult("文件不能为空");
        }
        // 获取文件名
        String fileName = file.getOriginalFilename();
        logger.info("上传的文件名为：" + fileName);
        // 文件上传后的路径
        String filePath = resourceLocation;
        // 解决中文问题，liunx下中文路径，图片显示问题
        // fileName = UUID.randomUUID() + suffixName;
        File dest = new File(filePath + fileName);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
            return buildResult(fileName);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buildFailResult("文件上传失败");
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public void getImage(@RequestParam String path, HttpServletResponse response) throws IOException {
        String filePath = resourceLocation;
        File f = new File(filePath + path);
        FileInputStream inputStream = new FileInputStream(f);
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        StreamUtils.copy(inputStream, response.getOutputStream());
    }


    @GetMapping("/code")
    @ResponseBody
    public ResultDataWrap sendCode(@RequestParam String mobile){
        //todo- 对接运营商发送手机短信,发送内容为random
        String random = StringUtil.getRandomNumCode(6);
        if(redisService.setMobileCode(mobile, random)) return buildResult();
        return buildFailResult();
    }


}
