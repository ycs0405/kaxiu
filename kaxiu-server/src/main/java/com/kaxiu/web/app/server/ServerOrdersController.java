package com.kaxiu.web.app.server;


import com.alibaba.fastjson.JSONObject;
import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.config.spring.annotation.ServerValidation;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.service.IOrdersService;
import com.kaxiu.util.LocationUtil;
import com.kaxiu.vo.ResultDataWrap;
import com.kaxiu.vo.enums.AttestationEnum;
import com.kaxiu.vo.order.OrderRequestVo;
import com.kaxiu.vo.order.OrderResponseVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 前端控制器
 * @ServerValidation 为服务人员状态验证 位于 com.kaxiu.config.spring.aspect.ServerValidationAspect
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/server/orders")
public class ServerOrdersController extends AbstractBaseController {

    @Resource
    private IOrdersService ordersService;

    @Resource
    private RedisService redisService;

    /**
     * server小程序使用接口
     * 获取我的订单
     * 正在进行
     * 已取消
     * 已结束
     * @return
     */
    @GetMapping(value = "/my")
    @ServerValidation
    public ResultDataWrap getMyOrders(@RequestParam Integer status,
                                      @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                      @RequestParam(required = false, defaultValue = "20") Integer pageSize){
        return buildResult(ordersService.getServerOrder(status, pageIndex, pageSize));
    }


    /**
     * server小程序使用接口
     * 正在进行的订单
     * @return
     */
    @GetMapping(value = "/underway")
    @ServerValidation
    public ResultDataWrap getMyOrders(){
        return buildResult(ordersService.getServerOrder());
    }

    /**
     * server小程序使用接口
     * 获取有效订单
     * @return
     */
    @GetMapping(value = "")
    @ServerValidation
    public ResultDataWrap getOrders(@RequestParam("location") String location){
        JSONObject locationJson = JSONObject.parseObject(location);
        //保存当前维修人员的位置信息
        redisService.setLocation(locationJson);
        List<OrderResponseVo> l = ordersService.getEffectiveOrders();
        l.stream().map( e -> {
            double distance = LocationUtil.distance(
                    locationJson.getDouble("latitude"),locationJson.getDouble("longitude"),
                    e.getLocation().getDouble("latitude"),
                    e.getLocation().getDouble("longitude"));
            e.setDistance(distance);
            return e;
        }).collect(Collectors.toList());
        return buildResult(l);
    }

    /**
     * server小程序使用接口
     * 接单
     * @return
     */
    @PostMapping(value = "", produces = "application/json;charset=UTF-8")
    @ServerValidation
    public ResultDataWrap takeOrder(@RequestBody JSONObject location){
        int result = ordersService.takeOrder(location.getLong("orderId"), JSONObject.parseObject(location.getString("location")));
        if(result > 0){
            return buildResult();
        }else{
            return buildFailResult("该订单已被其他人抢走！");
        }
    }

    /**
     * server小程序使用接口
     * 结束订单
     * @return
     */
    @PostMapping(value = "/over", produces = "application/json;charset=UTF-8")
    @ServerValidation
    public ResultDataWrap overOrder(@RequestBody JSONObject location){
        String imgFrontPath = location.getString("imgFrontPath");
        String imgOverPath = location.getString("imgOverPath");
        String orderId = location.getString("orderId");
        return buildResult(ordersService.overOrder(orderId, imgFrontPath, imgOverPath));
    }

}

